## Data Sources

For original / canon information, the following sources are used:

* Files ``pokedex_pokemon.${lang}.dic`` are fed from Bulbapedia's page of "List of Pokémon in other languages" and from a veekun schema-compatible database listing.
* Files ``moves.${lang}.dic`` are fed from Bulbapedia's page of "List of moves in other languages" and from a veekun schema-compatible database listing.
* Other files are fed from Bulbapedia's tabular data pages on internationalization, such as ["List of locations in other languages"](https://bulbapedia.bulbagarden.net/wiki/List_of_locations_in_other_languages)

The following SQL query, applied to a Veekun "pokedex" schema, extracts in its second column the names for Pokémon in a given ISO language code ("``{lang}``"):

```
select P.id, PN.name
FROM
pokemon P 
left join pokemon_species_names PN 
on PN.pokemon_species_id = P.id  
join languages L on PN.local_language_id=L.id
and L.identifier={lang}
where P.id is not null 
order by P.id, L.id
```
