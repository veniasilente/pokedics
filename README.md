# pokedics

Dictionary files for Pokémon-related stuff, covering Pokémon names, item names and location names up to at least G8 where possible. 

Also includes a section with fanon dictionaries.

Compatible with eg.: LibreOffice.

## Contents

* ``original`` contains dictionaries only for the released games in the franchise, evolved from the copy of the original dictionaries first published in 4chan's ``/vpwt/`` thread in 2019.
* ``supplements`` contains dictionaries for various supplemental and fanon material, such as Suocéverse specific Pokémon and terms, or unreleased Pokémon designs from the leaks.


Dictionary files follow a naming schema of the form ``{content}.{lang}.dic``, where:

* ``{content}`` is a name describing the category of content that the dictionary covers: Pokémon, moves, items, locations, etc.
* ``{lang}`` is a [ISO 639-1 language code](https://www.loc.gov/standards/iso639-2/php/English_list.php) for the language covered.

## Notes

* Dictionaries for Spanish ("es") and Italian ("it") official Pokémon names are **currently pending**, as it was not until G9's release of Paradox forms that the "it" and "es" internationalization teams started diverging from the "en" namelist.

* Files with the prefix ``pkunreleased`` contain names for unreleased entities, such as the leaked "beta Pokémon". 

* Where possible, data sources are credited in a folder-level ``SOURCES.md`` or in a per-source associated CSV file.

## License

These contents are licensed under **Creative Commons** NC-SA-4.0. For more details check ``LICENSE``.


